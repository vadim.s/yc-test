const express = require('express');
const { uuid } = require('uuidv4');
const request = require('request');

const router = express.Router();

router.get('/', (req, res) => {
  res.render('index');
});

router.get('/checkout', async (req, res) => {
  const { price, description } = req.query;

  const options = {
    method: 'POST',
    url: 'https://payment.yandex.net/api/v3/payments',
    headers: {
      'Idempotence-Key': uuid(),
      'Content-Type': ['application/json', 'text/plain'],
      Authorization:
        'Basic NTQ0MDE6dGVzdF9GaDhoVUFWVkJHVUdiam1semJhNlRCMGl5VWJvc19sdWVUSEUtYXhPd00w',
    },
    body: JSON.stringify({
      amount: {
        value: price,
        currency: 'RUB',
      },
      confirmation: {
        type: 'embedded',
      },
      capture: true,
      description,
    }),
  };

  request(options, (error, response) => {
    if (error) throw new Error(error);
    const responseObject = JSON.parse(response.body);

    const token = responseObject.confirmation.confirmation_token;
    const { id } = responseObject;
    res.render('checkout', { token, id, description });
  });
});

router.get('/result', (req, res) => {
  const { id } = req.query;

  const options = {
    method: 'GET',
    url: `https://payment.yandex.net/api/v3/payments/${id}`,
    headers: {
      Authorization:
        'Basic NTQ0MDE6dGVzdF9GaDhoVUFWVkJHVUdiam1semJhNlRCMGl5VWJvc19sdWVUSEUtYXhPd00w',
    },
  };

  request(options, (error, response) => {
    if (error) throw new Error(error);
    const responseObject = JSON.parse(response.body);

    const { status } = responseObject;
    res.render('result', { status });
  });
});

module.exports = router;
