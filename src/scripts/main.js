import '../styles/main.scss';

const checkout = new window.YandexCheckout({
  confirmation_token: localData, // Токен, который перед проведением оплаты нужно получить от Яндекс.Кассы
  return_url: `/result/?id=${paymentID}`, // Ссылка на страницу завершения оплаты
  error_callback(error) {
    alert(error);
  },
});

checkout.render('payment-form');
